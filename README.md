# Cortex Editor

Integrates a rich editor into the Cortex boilerplate profile.

## Features:
* Adds 2 editors (basic_html & restricted html) + matching formats, and a plain text format.
* Adds the possiblity to select a media entity as link path (for downloadable content)
* LinkIt Integration for easy internal linking of content.

**Note:**
The editor config will not override existing config, except for `basic_html`, 
which is overriden in the install step.

## Dependencies and Used Modules for Drupal
* [CKEditor Anchor Link](https://www.drupal.org/project/anchor_link)
* [CKEditor List Style](https://www.drupal.org/project/ckeditor_liststyle)
* [Editor Advanced link](https://www.drupal.org/project/editor_advanced_link)
* [External Links](https://www.drupal.org/project/extlink)
* [Linkit](https://www.drupal.org/project/linkit)
* [Linkit Media Library](https://www.drupal.org/project/linkit_media_library)

## Use With Cortex profile:
This module is best used with the Cortex installation profile, but can be 
installed on any Drupal 8/9/10 site.

## Credits
This module is developed by [openup.media](https://www.drupal.org/open-up-media).

This module is heavily instpired by [Vardot Editor](https://www.drupal.org/project/vardot_editor) by [Vardot](https://www.drupal.org/vardot)
